#!/usr/bin/python3
import sys

from PyQt5.QtWidgets import QApplication

def startapp():
    app = QApplication(sys.argv)

    from labs.lab import Labs

    l = Labs()
    l.show()

    sys.exit(app.exec_())


if __name__ == '__main__':
    startapp()

