import numpy as np

init_cond = 1
init_coord = 0
lab_func = lambda y, t: y + t ** 3
lab_sol = lambda t: -t ** 3 - 3 * t ** 2 - 6 * t + 7 * np.exp(t) - 6


def analytic(function, grid, initval):
    return [lab_sol(i) for i in grid]

def euler(function, grid, initval):
    """
        y'(t) = y(t)
        y(0) = 0
        y(t) = 0 * e^t = 0
    """

    def values(x_list, y_list):
        if len(x_list) <= len(y_list):
            return y_list
        else:
            hi = (x_list[len(y_list)] - x_list[len(y_list) - 1])
            y_next = y_list[-1] + hi * function(y_list[-1], x_list[len(y_list)])
            return values(x_list, np.append(y_list, y_next))

    return values(grid, [initval])


def runge_kutta_2_order(function, grid, initval, alpha=.5):
    def values(x_list, y_list):
        if len(x_list) <= len(y_list):
            return y_list
        else:
            hi = (x_list[len(y_list)] - x_list[len(y_list) - 1])

            fst_ord = (1 - alpha) * function(y_list[-1], x_list[len(y_list)])
            snd_ord = alpha * function(
                y=y_list[-1] + hi / (2 * alpha) * function(y_list[-1], x_list[len(y_list)]),
                t=x_list[len(y_list)] + hi / (2 * alpha),
            )

            y_next = y_list[-1] + hi * (fst_ord + snd_ord)
            return values(x_list, np.append(y_list, y_next))

    return values(grid, [initval])


def runge_kutta_4_order(function, grid, initval):
    def values(x_list, y_list):
        if len(x_list) <= len(y_list):
            return y_list
        else:
            hi = (x_list[len(y_list)] - x_list[len(y_list) - 1])

            k1 = function(y_list[-1], x_list[len(y_list)])
            k2 = function(y_list[-1] + hi / 2 * k1, x_list[len(y_list)] + hi / 2)
            k3 = function(y_list[-1] + hi / 2 * k2, x_list[len(y_list)] + hi / 2)
            k4 = function(y_list[-1] + hi / 2 * k3, x_list[len(y_list)] + hi / 2)

            y_next = y_list[-1] + hi / 6 * (k1 + 2 * k2 + 2 * k3 + k4)
            return values(x_list, np.append(y_list, y_next))

    return values(grid, [initval])
