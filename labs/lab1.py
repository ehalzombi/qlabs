from PyQt5.QtWidgets import (QWidget, QTableWidget, QPushButton, QCheckBox,
                             QLineEdit, QLabel, QHBoxLayout, QVBoxLayout)
from PyQt5.QtChart import QChart, QChartView, QLineSeries, QScatterSeries
from PyQt5.QtCore import pyqtSignal, Qt

from .approx_alg import newton, lagrange, msq, spline

import numpy as np

class Lab1(QWidget):

    def __init__(self):
        super().__init__()

        self.initUI()
        self.initConnections()
        self.initPlot()
        self.initTable()

    def initUI(self):

        left_point_label = QLabel('Левая граница')
        right_point_label = QLabel('Правая граница')

        plotSetWidthLayout = QHBoxLayout()
        plotSetWidthLayout.addWidget(left_point_label)
        plotSetWidthLayout.addWidget(self.left_point)
        plotSetWidthLayout.addWidget(right_point_label)
        plotSetWidthLayout.addWidget(self.right_point)

        plotButtonsLayout = QHBoxLayout()
        for lab in self.labs_data:
            plotButtonsLayout.addWidget(lab["box"])

        buttonLayout = QVBoxLayout()
        buttonLayout.addWidget(self.drawButton)
        buttonLayout.addWidget(self.addCell)
        buttonLayout.addWidget(self.removeCell)

        dataLayout = QHBoxLayout()
        dataLayout.addWidget(self.dataTable)
        dataLayout.addLayout(buttonLayout)

        mainLayout = QVBoxLayout()
        mainLayout.addWidget(self.chartView)
        mainLayout.addLayout(plotSetWidthLayout)
        mainLayout.addLayout(plotButtonsLayout)
        mainLayout.addLayout(dataLayout)

        self.setLayout(mainLayout)

    def initConnections(self):
        self.addCell.clicked.connect(self.addColumn)
        self.removeCell.clicked.connect(self.removeColumn)
        self.drawButton.clicked.connect(self.redrawPlot)


        for lab in self.labs_data:
            def switchActive(lab, active):
                lab["active"]=active
            lab["box"].stateChanged.connect(lambda i, lab=lab: switchActive(lab, bool(i)))

    def initTable(self):

        self.dataTable.setMaximumHeight(100)

    def addColumn(self):

        self.dataTable.insertColumn(
            self.dataTable.columnCount()
        )

    def removeColumn(self):

        self.dataTable.removeColumn(
            self.dataTable.columnCount() - 1
        )

    def initPlot(self):

        for lab in self.labs_data:
            lab["line"].setName(lab["name"])
            self.chart.addSeries(lab["line"])

        self.scatterSeries.setName('Исходные точки')
        self.chart.addSeries(self.scatterSeries)

        self.chart.legend().setAlignment(Qt.AlignBottom)
        self.chart.createDefaultAxes()

        self.chartView.setChart(self.chart)

    def redrawPlot(self):
        x, y = [], []
        for i in np.arange(self.dataTable.columnCount()):

            if self.dataTable.item(0, i) == None or self.dataTable.item(1, i) == None:
                continue

            x.append(float(self.dataTable.item(0, i).text()))
            y.append(float(self.dataTable.item(1, i).text()))

        xmin, xmax = 0, 0

        if self.left_point.text() != '':
            xmin = float(self.left_point.text())
        else:
            xmin = np.min(x)

        if self.right_point.text() != '':
            xmax = float(self.right_point.text())
        else:
            xmax = np.max(x)
        step = (xmax - xmin) / 300.
        xx = np.arange(xmin, xmax+step, step)

        self.scatterSeries.clear()
        for aa, bb in zip(x, y):
            self.scatterSeries.append(aa, bb)

        ymin, ymax = np.min(y), np.max(y)

        for lab in self.labs_data:
            lab["line"].clear()
            if lab["active"] == False:
                continue
            poly = lab["function"](x=x, y=y)

            yy = np.array([poly(i) for i in xx])
            ymin = np.min([ymin, np.min(yy)])
            ymax = np.max([ymax, np.max(yy)])

            for aa, bb in zip(xx, yy):
                lab["line"].append(aa, bb)

        self.chart.axisX().setRange(np.min([np.min(x), np.min(xx)]),
                                    np.max([np.max(x), np.max(xx)]))
        self.chart.axisY().setRange(ymin, ymax)
        self.chartView.repaint()

    redraw = pyqtSignal([list], [list])

    # Plot
    labs_data = [
        {
            'name': 'Метод Ньютона',
            'box': QCheckBox('Метод Ньютона'),
            'function': newton,
            'line': QLineSeries(),
            'active': False
        },
        {
            'name': 'Метод Лагранжа',
            'box': QCheckBox('Метод Лагранжа'),
            'function': lagrange,
            'line': QLineSeries(),
            'active': False
        },
        {
            'name': 'Метод наименьших квадратов',
            'box': QCheckBox('Метод наименьших квадратов'),
            'function': msq,
            'line': QLineSeries(),
            'active': False
        },
        {
            'name': 'Интерполяция сплайнами',
            'box': QCheckBox('Интерполяция сплайнами'),
            'function': spline,
            'line': QLineSeries(),
            'active': False
        }
    ]

    scatterSeries = QScatterSeries()
    chart = QChart()
    chartView = QChartView()

    left_point = QLineEdit()
    right_point = QLineEdit()

    # UI
    dataTable = QTableWidget(2, 4)

    addCell = QPushButton('Добавить точку')
    removeCell = QPushButton('Удалить точку')
    drawButton = QPushButton('Нарисовать график')

