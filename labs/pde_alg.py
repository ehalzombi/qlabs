import numpy as np


def U_t0(wave_num=0, amplitude=1):
    return lambda x: amplitude * np.abs(np.sin((np.pi * wave_num * x) / x[-1]))


def U_xa(k=1., alpha=1., U0=20.):
    return lambda y_prev, y_feedback, xh: (k * xh)* (
        (U0 - alpha * y_feedback) ** 2) * np.heaviside(U0 - alpha * y_feedback, .5) + y_prev


settings = {
    'k': 1,
    'U_t0': U_t0(wave_num=4),
    'U_xa': U_xa(k=1, U0=20, alpha=1),
    'xh': .1,
    'th': 0.005,
    'a2': 4,
    'T2': 80,
}


def pde(settings):
    # ---
    xh = settings.pop('xh', 0.1)
    th = settings.pop('th', 0.002)

    k = settings.pop('k', 1)

    a1 = settings.pop('a1', 0)
    a2 = settings.pop('a2', 1)
    T1 = settings.pop('T1', 0)
    T2 = settings.pop('T2', 1)

    U_t0 = settings.pop('U_t0', lambda x: .5)
    U_x0 = settings.pop('U_x0', lambda t: 0)
    U_xa = settings.pop('U_xa', lambda t: 0)

    # ---
    x = np.arange(a1, a2, xh)
    t = np.arange(T1, T2, th)

    feedback_index = int(len(x) / 2)

    def next_layer(layer, time=0):
        def next_layer_item(layer, index):
            return layer[index] + k * th / (xh ** 2) * (layer[index + 1] - 2 * layer[index] + layer[index - 1])

        result_layer = np.empty([len(layer)])
        result_layer[0] = U_x0(time)
        result_layer[-1] = U_xa(y_prev=layer[-2], y_feedback=layer[feedback_index], xh=xh)

        for i in np.arange(1, len(layer) - 1):
            result_layer[i] = next_layer_item(layer=layer, index=i)

        return result_layer

    u = np.empty([1, len(x)])
    u[0, :] = U_t0(x)

    for time in t:
        u = np.vstack([u, next_layer(u[-1, :], time)])

    return (x, t, u)

