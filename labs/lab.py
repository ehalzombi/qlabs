from PyQt5.QtWidgets import QMainWindow, QWidget, QTabWidget, QHBoxLayout, QVBoxLayout

from .lab1 import Lab1
from .lab2 import Lab2
from .lab3 import Lab3

class Labs(QWidget):

    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):

        tabWidget = QTabWidget()

        l1 = Lab1()
        tabWidget.addTab(l1, 'Интерполяция')

        l2 = Lab2()
        tabWidget.addTab(l2, 'Дифференциальные уравнения')

        l3 = Lab3()
        tabWidget.addTab(l3, 'Дифференциальные уравнения в частных производных')


        mainLayout = QVBoxLayout()
        mainLayout.addWidget(tabWidget)
        self.setLayout(mainLayout)

        self.move(50, 50)
