from PyQt5.QtWidgets import (QWidget, QVBoxLayout, QHBoxLayout,
                             QCheckBox, QPushButton, QLineEdit, QLabel)
from PyQt5.QtChart import QChart, QChartView, QLineSeries, QScatterSeries
from PyQt5.QtCore import Qt

import numpy as np

from . import diff_eq_alg


class Lab2(QWidget):
    def __init__(self):
        super(Lab2, self).__init__()


        self.initUI()
        self.initConnections()
        self.initPlot()

    def initUI(self):
        mainLayout = QVBoxLayout()

        mainLayout.addWidget(self.chartView)

        settingsLayout = QHBoxLayout()

        endGridLayout = QVBoxLayout()
        endGridLayout.addWidget(QLabel('Конец сетки'))
        endGridLayout.addWidget(self.end_grid)

        stepCountLayout = QVBoxLayout()
        stepCountLayout.addWidget(QLabel('Количество ячеек в сетке'))
        stepCountLayout.addWidget(self.step_count)

        settingsLayout.addLayout(endGridLayout)
        settingsLayout.addLayout(stepCountLayout)
        settingsLayout.addWidget(self.redraw_plot_button)

        boxLayout = QHBoxLayout()
        for lab in self.lab_data:
            boxLayout.addWidget(lab['box'])

        mainLayout.addLayout(boxLayout)
        mainLayout.addLayout(settingsLayout)
        self.setLayout(mainLayout)

    def initPlot(self):
        for lab in self.lab_data:
            lab["line"].setName(lab["name"])
            self.chart.addSeries(lab["line"])

        self.chart.legend().setAlignment(Qt.AlignBottom)
        self.chart.setTitle('y\'(t)=y(t)+t^3')
        self.chart.createDefaultAxes()

        self.chartView.setChart(self.chart)

    def initConnections(self):
        self.redraw_plot_button.clicked.connect(self.redrawPlot)

        for lab in self.lab_data:
            def switchActive(lab, active):
                lab["active"]=active
            lab["box"].stateChanged.connect(lambda i, lab=lab: switchActive(lab, bool(i)))


    def redrawPlot(self):
        x_right = float(self.end_grid.text())
        step_count = int(self.step_count.text())

        grid = np.linspace(diff_eq_alg.init_coord, x_right, step_count)
        y_min, y_max = 0, 0

        for lab in self.lab_data:
            lab['line'].clear()
            if lab['active'] == False:
                continue

            y_list = lab['function'](function=diff_eq_alg.lab_func,
                                     grid=grid,
                                     initval=diff_eq_alg.init_cond)

            if y_min > np.min(y_list):
                y_min = np.min(y_list)
            if y_max < np.max(y_list):
                y_max = np.max(y_list)

            for x, y in zip(grid, y_list):
                lab['line'].append(x, y)

        self.chart.axisX().setRange(np.min(grid), np.max(grid))
        self.chart.axisY().setRange(y_min, y_max)
        self.chartView.repaint()

    chart = QChart()
    chartView = QChartView()

    redraw_plot_button = QPushButton('Нарисовать')

    end_grid, step_count = QLineEdit(), QLineEdit()

    """
    y'(t) = y(t) + t^3
    y(0) = 1
    ---
    y(t) = -t^3 - 3t^2 - 6t +7e^t - 6
    """

    lab_data = [
        {
            'name': 'Аналитическое решение',
            'box': QCheckBox('Аналитическое решение'),
            'function': diff_eq_alg.analytic,
            'line': QLineSeries(),
            'active': False,
        },
        {
            'name': 'Метод Эйлера',
            'box': QCheckBox('Метод Эйлера'),
            'function': diff_eq_alg.euler,
            'line': QLineSeries(),
            'active': False,
        },
        {
            'name': 'Метод Рунге-Кутты 2 порядка',
            'box': QCheckBox('Метод Рунге-Кутты 2 порядка'),
            'function': diff_eq_alg.runge_kutta_2_order,
            'line': QLineSeries(),
            'active': False,
        },
        {
            'name': 'Метод Рунге-Кутты 4 порядка',
            'box': QCheckBox('Метод Рунге-Кутты 4 порядка'),
            'function': diff_eq_alg.runge_kutta_4_order,
            'line': QLineSeries(),
            'active': False,
        },
    ]
